CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Google Calendar Import module provides a way to import Google Calendar
events from a publicly available calendar into Drupal.

Once into Drupal, you can layer on additional fields, theming, access control,
and all the other things that make Drupal Entities so excellent to work with.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/google_calendar

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/google_calendar


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Google Calendar Import module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to admin/google-calendar/calendar to add a Google calendar.
    3. Enter the name of the calendar and the ID (This can be obtained from the
       "Integrate Calendar" section of your calendar's settings.)
    4. Save.


MAINTAINERS
-----------

 * Drew Trafton (dtraft) - https://www.drupal.org/u/dtraft
